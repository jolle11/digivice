const fetchDigimonList = async () => {
    const url = 'https://digimon-api.vercel.app/api/digimon';
    const response = await fetch(url);
    const digimon = await response.json();
    console.log(digimon);
    return digimon;
};

export { fetchDigimonList };
