import { DigimonClass } from '../models/digimon';

const fetchDigimonDetail = async digiName => {
    const url = `https://digimon-api.vercel.app/api/digimon/name/${digiName}`;
    const response = await fetch(url);
    const data = await response.json();
    let digimon = new DigimonClass(data[0].name, data[0].img, data[0].level);
    return digimon;
};

export { fetchDigimonDetail };
