class DigimonClass {
    constructor(name, img, level) {
        this.name = name;
        this.img = img;
        this.level = level;
    }
    getDigimonName() {
        return this.name;
    }
    getDigimonImg() {
        return this.img;
    }
    getDigimonLevel() {
        return this.level;
    }
}

export { DigimonClass };
