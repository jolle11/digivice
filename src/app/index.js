import './styles/styles.scss';
import 'bootstrap';
import { displayDigimonList } from '../app/views/list-view';
import { displayDigimonDetail } from '../app/views/detail-view';
import { displayRandomDigimon } from '../app/views/random-view';

const addListeners = () => {
    document.getElementById('btnList').addEventListener('click', displayDigimonList);
    document.getElementById('btnRandom').addEventListener('click', displayRandomDigimon);
    document.getElementById('btnSearch').addEventListener('click', displayDigimonDetail);
    document.getElementById('btnClear').addEventListener('click', () => {
        document.getElementById('digivice').innerHTML = '';
        document.getElementById('digiName').value = '';
    });
};

window.onload = () => {
    addListeners();
};
