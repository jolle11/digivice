import { fetchDigimonList } from '../api/call-api-list';

const displayRandomDigimon = async () => {
    document.getElementById('digivice').innerHTML = '';
    const digimonListPromise = fetchDigimonList();
    digimonListPromise.then(digimonList => {
        let randomNum = Math.floor(Math.random() * (209 - 0) + 0);
        const digimonHTMLString = `
        <li class="flex-detail">
            <img class="digimon-detail-img" src="${digimonList[randomNum].img}"/>
            <h2 class="">${digimonList[randomNum].name}</h2>
            <p class="">Level: ${digimonList[randomNum].level}</p>
        </li>
        `;
        digivice.innerHTML = digimonHTMLString;
    });
};

export { displayRandomDigimon };
