import { fetchDigimonList } from '../api/call-api-list';

const displayDigimonList = async () => {
    document.getElementById('digivice').innerHTML = '';
    const digimonListPromise = fetchDigimonList();
    digimonListPromise.then(digimonList => {
        digimonList.forEach(digimon => {
            const li = document.createElement('li');
            li.classList.add('flex-list');
            const img = document.createElement('img');
            img.src = digimon.img;
            img.classList.add('digimon-list-img');
            li.appendChild(img);
            const h3 = document.createElement('h3');
            h3.innerText = digimon.name;
            li.appendChild(h3);
            digivice.appendChild(li);
        });
    });
};

export { displayDigimonList };
