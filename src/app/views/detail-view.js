import { fetchDigimonDetail } from '../api/call-api-detail';
import { DigimonClass } from '../models/digimon';

const displayDigimonDetail = async () => {
    const digivice = document.getElementById('digivice');
    const digiName = document.getElementById('digiName').value;
    const digimon = await fetchDigimonDetail(digiName);
    const digimonDetail = new DigimonClass(digimon.name, digimon.img, digimon.level);
    const digimonHTMLString = `
    <li class="flex-detail">
        <img class="digimon-detail-img" src="${digimonDetail.getDigimonImg()}"/>
        <h2 class="">${digimonDetail.getDigimonName()}</h2>
        <p class="">Level: ${digimonDetail.getDigimonLevel()}</p>
    </li>
    `;
    digivice.innerHTML = digimonHTMLString;
};

export { displayDigimonDetail };
