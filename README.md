# Digivice APP

![Logo Digimon](./src/app/assets/Digimon_Logo.svg.png)

# Objetivo

Construir una aplicacion web combinando todo lo aprendido de HTML/CSS y Javascript.

El objetivo es enriquecer y mejorar la aplicación base, para entender el comportamiento de cada componente en nuestra app.

## API

Para mi app he usado la [API pública de Digimon](https://digimon-api.herokuapp.com/) para listar y consultar el detalle de un Digimon concreto.

## Stack tecnológico

-   Node + NPM
-   Webpack + Babel
-   HTML/CSS
-   Bootstrap
-   Sass
-   Javascript (ES6)

## Reto

Para mi fue todo un reto la construcción de la app ya que esto de las API's era algo muy nuevo para mi. Estoy contento porque ha salido algo básico pero decente con los conocimientos bastate bien aplicados.
